package in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin.model;

import java.io.Serializable;

/**
 * Created by Skaha_AM on 19-Feb-18.
 */

public class Hadiah implements Serializable {
    public String judul, deskripsi, detail, lokasi, foto;

    public Hadiah(String judul, String deskripsi, String detail, String lokasi, String foto) {
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.detail = detail;
        this.lokasi = lokasi;
        this.foto = foto;
    }
}
