package in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin.R;


public class SliderAdapter extends PagerAdapter {
    public String[] slideTextSatu = {"Welcome", "Special Gift", "Yummy Cake", "Pretty Decoration", "Lovely Wrap"};
    public String[] slideTextDua = {
            "Hadiah.IN",
            "Merekomendasikan hadiah yang tepat untuk seseorang yang tepat!",
            "Kue lezat dengan topping yang menggemaskan dengan kreasi sendiri!",
            "Dekorasi impian, menyempurnakan pesta meriah anda!",
            "Bungkus kado, dengan cinta penuh kasih sayang!"};
    public String[] slideTextTiga = {
            "I hope this helps!",
            "How does this work?",
            "How can?",
            "What's Decoration?",
            "What does it do?"};
    public String[] slideTextEmpat = {
            "Aplikasi Hadiah.IN ini merupakan aplikasi yang dapat merekomendasikan hadiah, dekorasi pesta dan tips-tips menghias kado, juga tutorial membuat kue yang enak untuk pesta anda",
            "Hadiah.IN akan menyesuaikan kado yang cocok dengan usia, gender, dan hal hal favorit orang yang akan anda kado serta menyesuaikan budget yang anda miliki",
            "Hadiah.IN memberikan tutorial kue lezat yang menjamin bentuk dan kelezatannya dan tentunya memuaskan, bukan mengecewakan",
            "Memberikan macam macam pilihan dekorasi sesuai budget yang anda miliki dan pastinya mewujudkan dekorasi impian anda",
            "Hadiah.IN memberikan tips-tips membungkus kado dengan cantik dan menjadi BEST GIFT WRAP EVER"};
    Context context;
    LayoutInflater layoutInflater;

    public SliderAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return slideTextSatu.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);
        TextView tvSatu = view.findViewById(R.id.txtSatu);
        TextView tvDua = view.findViewById(R.id.txtDua);
        TextView tvTiga = view.findViewById(R.id.txtTiga);
        TextView tvEmpat = view.findViewById(R.id.txtEmpat);
        tvSatu.setText(slideTextSatu[position]);
        tvDua.setText(slideTextDua[position]);
        tvTiga.setText(slideTextTiga[position]);
        tvEmpat.setText(slideTextEmpat[position]);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
