package in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin.model.tampilMode;

public class TampilActivity extends AppCompatActivity {
    ArrayList<tampilMode> tampilModes = new ArrayList<>();


    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tampil);
        recyclerView = findViewById(R.id.recycler);

        String ref = getIntent().getExtras().getString("Ref");
        Log.d("HadiahIn", "onDataChange: " + ref);
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference dbRef = firebaseDatabase.getReference(ref);

        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (int i = 1; i <= 9; i++) {
                    tampilMode tampilMode = new tampilMode();
                    tampilMode.judul = dataSnapshot.child("judul" + i).getValue().toString();
                    tampilMode.desc = dataSnapshot.child("desc" + i).getValue().toString();
                    tampilMode.img = dataSnapshot.child("gambar" + i).getValue().toString();

                    tampilModes.add(tampilMode);
                }

                CreateRecycle();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("HadiahIn", "onDataChange: " + databaseError.toString());
            }
        });
    }

    public void CreateRecycle() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        tampilAdapter tampilAdapter = new tampilAdapter(tampilModes, this);
        recyclerView.setAdapter(tampilAdapter);
    }


}
