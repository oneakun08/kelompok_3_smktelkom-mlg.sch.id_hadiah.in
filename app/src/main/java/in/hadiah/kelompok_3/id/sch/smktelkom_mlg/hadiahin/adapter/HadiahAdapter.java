package in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin.R;
import in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin.model.Hadiah;

/**
 * Created by Skaha_AM on 19-Feb-18.
 */

public class HadiahAdapter extends RecyclerView.Adapter<HadiahAdapter.ViewHolder> {
    ArrayList<Hadiah> hadiahList;
    IHadiahAdapter mIHadiahAdapter;

    public HadiahAdapter(Context context, ArrayList<Hadiah> hadiahList) {
        this.hadiahList = hadiahList;
        mIHadiahAdapter = (IHadiahAdapter) context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Hadiah hadiah = hadiahList.get(position);
        holder.tvJudul.setText(hadiah.judul);
        holder.ivFoto.setImageURI(Uri.parse(hadiah.foto));
    }

    @Override
    public int getItemCount() {
        if (hadiahList != null)
            return hadiahList.size();
        return 0;
    }

    public interface IHadiahAdapter {
        void doClick(int pos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ivFoto;
        TextView tvJudul, tvDeskripsi;
        Button bEdit, bDelete;
        ImageButton ibFav, ibShare;

        public ViewHolder(View itemView) {
            super(itemView);
            ivFoto = itemView.findViewById(R.id.imageView);
            tvJudul = itemView.findViewById(R.id.textViewJudul);
            tvDeskripsi = itemView.findViewById(R.id.textViewDeskripsi);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mIHadiahAdapter.doClick(getAdapterPosition());
                }
            });
        }
    }
}
