package in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class YummiCake extends AppCompatActivity {
    TextView judul, description;
    ImageView img, img2, img3, img4, img6, img7, img8, img9, imga, imgb, imgc, imgd, imge, imgf, imgg, imgh, imgi, imgj, imgk, imgl, imgm, imgn, imgo, imgp, imgq, imgr, imgs, imgt, imgu, imgv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yummi_cake);

        judul = findViewById(R.id.judul);
        description = findViewById(R.id.descripsi);
        img = findViewById(R.id.img);
        img2 = findViewById(R.id.img2);
        img3 = findViewById(R.id.img3);
        img4 = findViewById(R.id.img4);
        img6 = findViewById(R.id.img6);
        img7 = findViewById(R.id.img7);
        img8 = findViewById(R.id.img8);
        img9 = findViewById(R.id.img9);
        imga = findViewById(R.id.imga);
        imgb = findViewById(R.id.imgb);
        imgc = findViewById(R.id.imgc);
        imgd = findViewById(R.id.imgd);
        imge = findViewById(R.id.imge);
        imgf = findViewById(R.id.imgf);
        imgg = findViewById(R.id.imgg);
        imgh = findViewById(R.id.imgh);
        imgi = findViewById(R.id.imgi);
        imgj = findViewById(R.id.imgj);
        imgk = findViewById(R.id.imgk);
        imgl = findViewById(R.id.imgl);
        imgm = findViewById(R.id.imgm);
        imgn = findViewById(R.id.imgn);
        imgo = findViewById(R.id.imgo);
        imgp = findViewById(R.id.imgp);
        imgq = findViewById(R.id.imgq);
        imgr = findViewById(R.id.imgr);
        imgs = findViewById(R.id.imgs);
        imgt = findViewById(R.id.imgt);
        imgu = findViewById(R.id.imgu);
        imgv = findViewById(R.id.imgv);

        getDataFromFirebase();
    }

    private void getDataFromFirebase() {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference dbRef = firebaseDatabase.getReference("yummy");
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                judul.setText(dataSnapshot.child("title").getValue().toString());
                description.setText(dataSnapshot.child("isi").getValue().toString());
                Picasso.with(YummiCake.this).load(dataSnapshot.child("img").getValue().toString()).into(img);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("img2").getValue().toString()).into(img2);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("img3").getValue().toString()).into(img3);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("img4").getValue().toString()).into(img4);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("img6").getValue().toString()).into(img6);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("img7").getValue().toString()).into(img7);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("img8").getValue().toString()).into(img8);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("img9").getValue().toString()).into(img9);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imga").getValue().toString()).into(imga);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgb").getValue().toString()).into(imgb);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgc").getValue().toString()).into(imgc);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgd").getValue().toString()).into(imgd);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imge").getValue().toString()).into(imge);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgf").getValue().toString()).into(imgf);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgg").getValue().toString()).into(imgg);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgh").getValue().toString()).into(imgh);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgi").getValue().toString()).into(imgi);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgj").getValue().toString()).into(imgj);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgk").getValue().toString()).into(imgk);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgl").getValue().toString()).into(imgl);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgm").getValue().toString()).into(imgm);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgn").getValue().toString()).into(imgn);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgo").getValue().toString()).into(imgo);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgp").getValue().toString()).into(imgp);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgq").getValue().toString()).into(imgq);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgr").getValue().toString()).into(imgr);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgs").getValue().toString()).into(imgs);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgt").getValue().toString()).into(imgt);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgu").getValue().toString()).into(imgu);
                Picasso.with(YummiCake.this).load(dataSnapshot.child("imgv").getValue().toString()).into(imgv);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
