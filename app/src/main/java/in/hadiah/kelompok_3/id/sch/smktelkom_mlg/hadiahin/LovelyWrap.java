package in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class LovelyWrap extends AppCompatActivity {

    TextView jdl1, jdl2, jdl3, jdl4, jdl5, jdl6, jdl7, jdl8, jdl9, desc1, desc2, desc3, desc4, desc5, desc6, desc7, desc8, desc9;
    ImageView al, an, any, bn, gb, km, kmn, kn, ts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lovely_wrap);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        jdl1 = findViewById(R.id.jdl1);
        jdl2 = findViewById(R.id.jdl2);
        jdl3 = findViewById(R.id.jdl3);
        jdl4 = findViewById(R.id.jdl4);
        jdl5 = findViewById(R.id.jdl5);
        jdl6 = findViewById(R.id.jdl6);
        jdl7 = findViewById(R.id.jdl7);
        jdl8 = findViewById(R.id.jdl8);
        jdl9 = findViewById(R.id.jdl9);


        desc1 = findViewById(R.id.desc1);
        desc2 = findViewById(R.id.desc2);
        desc3 = findViewById(R.id.desc3);
        desc4 = findViewById(R.id.desc4);
        desc5 = findViewById(R.id.desc5);
        desc6 = findViewById(R.id.desc6);
        desc7 = findViewById(R.id.desc7);
        desc8 = findViewById(R.id.desc8);
        desc9 = findViewById(R.id.desc9);

        al = findViewById(R.id.al);
        an = findViewById(R.id.an);
        any = findViewById(R.id.any);
        bn = findViewById(R.id.bn);
        gb = findViewById(R.id.gb);
        km = findViewById(R.id.km);
        kmn = findViewById(R.id.kmn);
        kn = findViewById(R.id.kn);
        ts = findViewById(R.id.ts);
        getDataFromFirebase();
    }

    private void getDataFromFirebase() {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference dbRef = firebaseDatabase.getReference("lovely");
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                jdl1.setText(dataSnapshot.child("jdl1").getValue().toString());
                jdl2.setText(dataSnapshot.child("jdl2").getValue().toString());
                jdl3.setText(dataSnapshot.child("jdl3").getValue().toString());
                jdl4.setText(dataSnapshot.child("jdl4").getValue().toString());
                jdl5.setText(dataSnapshot.child("jdl5").getValue().toString());
                jdl6.setText(dataSnapshot.child("jdl6").getValue().toString());
                jdl7.setText(dataSnapshot.child("jdl7").getValue().toString());
                jdl8.setText(dataSnapshot.child("jdl8").getValue().toString());
                jdl9.setText(dataSnapshot.child("jdl9").getValue().toString());

                desc1.setText(dataSnapshot.child("desc1").getValue().toString());
                desc2.setText(dataSnapshot.child("desc2").getValue().toString());
                desc3.setText(dataSnapshot.child("desc3").getValue().toString());
                desc4.setText(dataSnapshot.child("desc4").getValue().toString());
                desc5.setText(dataSnapshot.child("desc5").getValue().toString());
                desc6.setText(dataSnapshot.child("desc6").getValue().toString());
                desc7.setText(dataSnapshot.child("desc7").getValue().toString());
                desc8.setText(dataSnapshot.child("desc8").getValue().toString());
                desc9.setText(dataSnapshot.child("desc9").getValue().toString());

                Picasso.with(LovelyWrap.this).load(dataSnapshot.child("al").getValue().toString()).into(al);
                Picasso.with(LovelyWrap.this).load(dataSnapshot.child("an").getValue().toString()).into(an);
                Picasso.with(LovelyWrap.this).load(dataSnapshot.child("any").getValue().toString()).into(any);
                Picasso.with(LovelyWrap.this).load(dataSnapshot.child("bn").getValue().toString()).into(al);
                Picasso.with(LovelyWrap.this).load(dataSnapshot.child("gb").getValue().toString()).into(an);
                Picasso.with(LovelyWrap.this).load(dataSnapshot.child("km").getValue().toString()).into(any);
                Picasso.with(LovelyWrap.this).load(dataSnapshot.child("kmn").getValue().toString()).into(al);
                Picasso.with(LovelyWrap.this).load(dataSnapshot.child("kn").getValue().toString()).into(an);
                Picasso.with(LovelyWrap.this).load(dataSnapshot.child("ts").getValue().toString()).into(any);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }
}
