package in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin.lihat.PaketActivity;
import in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin.model.Hadiah;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static final String HADIAH = "hadiah";
    private static final int REQUEST_CODE_ADD = 88;
    private static final int REQUEST_CODE_EDIT = 99;
    ViewPager viewPager;
    PagerAdapter adapter;
    String[] judul;
    String[] keterangan;
    int[] thumb;
    ArrayList<Hadiah> mList = new ArrayList<>();
    private TextView sgifa, love, yummi, pretty;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private Toolbar toolbar;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sgifa = findViewById(R.id.sgifa);
        love = findViewById(R.id.love);
        yummi = findViewById(R.id.yummi);
        pretty = findViewById(R.id.pretty);
        //array dari isi wisatanya
        judul = new String[]{"Kue Ultah Enak", "Kue Buto keren"};
        int thumb[] = {R.drawable.uc, R.drawable.hh1};
        keterangan = new String[]{"< 1 Geser Kiri >", " 3 Geser Kanan"};

        mDrawerLayout = findViewById(R.id.drawer);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.Open, R.string.Close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sgifa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SgiftActivity.class);
                startActivity(intent);
            }
        });
        love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LovelyWrap.class);
                startActivity(intent);
            }
        });
        yummi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, YummiCake.class);
                startActivity(intent);
            }
        });
        pretty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PrettyDecoration.class);
                startActivity(intent);
            }
        });
        //TAMBAHAN
        setNavigationViewListener();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    //ASLINE IKI
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.dashboard: {
                Toast.makeText(getApplicationContext(), "Daftar Aplikasi Dipilih", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
                return true;
            }
            case R.id.about: {
                Toast.makeText(getApplicationContext(), "Daftar Aplikasi Dipilih", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), AboutActivity.class);
                startActivity(i);
                finish();
                return true;
            }
            case R.id.mvp: {
                Toast.makeText(getApplicationContext(), "Daftar Aplikasi Dipilih", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
                startActivity(i);
                finish();
                return true;
            }
            case R.id.help: {
                Toast.makeText(getApplicationContext(), "Daftar Aplikasi Dipilih", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), HelpActivity.class);
                startActivity(i);
                finish();
                return true;
            }
            case R.id.ms: {
                Toast.makeText(getApplicationContext(), "Daftar Aplikasi Dipilih", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), PaketActivity.class);
                startActivity(i);
                finish();
                return true;
            }
            case R.id.logout: {
                Toast.makeText(getApplicationContext(), "Daftar Aplikasi Dipilih", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                return false;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void setNavigationViewListener() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //  View header = navigationView.inflateHeaderView(R.layout.header);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
        return;
    }
}
