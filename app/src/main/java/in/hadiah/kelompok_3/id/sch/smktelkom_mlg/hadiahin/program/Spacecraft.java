package in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin.program;

/**
 * Created by Skaha_AM on 03-May-18.
 */

public class Spacecraft {
    String name;

    public Spacecraft() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
