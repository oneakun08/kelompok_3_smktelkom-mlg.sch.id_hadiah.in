package in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, Walkthrough.class);
                startActivity(i);
                //jeda selesai Splashscreen
                finish();
            }
        }, 5000);
    }
}
