package in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class PrettyDecoration extends AppCompatActivity {

    ImageView dekorasi1, dekorasi2, dekorasi3, dekorasi4, dekorasi5;
    TextView des1, des2, des3, des4, des5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pretty_decoration);

        dekorasi1 = findViewById(R.id.dekorasi1);
        des1 = findViewById(R.id.des1);
        dekorasi2 = findViewById(R.id.dekorasi2);
        des2 = findViewById(R.id.des2);
        dekorasi3 = findViewById(R.id.dekorasi3);
        des3 = findViewById(R.id.des3);
        dekorasi4 = findViewById(R.id.dekorasi4);
        des4 = findViewById(R.id.des4);
        dekorasi5 = findViewById(R.id.dekorasi5);
        des5 = findViewById(R.id.des5);

        getDataFromFirebase();

    }

    private void getDataFromFirebase() {


        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference dbRef = firebaseDatabase.getReference("pretty");
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                des1.setText(dataSnapshot.child("des1").getValue().toString());
                des2.setText(dataSnapshot.child("des2").getValue().toString());
                des3.setText(dataSnapshot.child("des3").getValue().toString());
                des4.setText(dataSnapshot.child("des4").getValue().toString());
                des5.setText(dataSnapshot.child("des5").getValue().toString());


                Picasso.with(PrettyDecoration.this).load(dataSnapshot.child("img1").getValue().toString()).into(dekorasi1);
                Picasso.with(PrettyDecoration.this).load(dataSnapshot.child("img2").getValue().toString()).into(dekorasi2);
                Picasso.with(PrettyDecoration.this).load(dataSnapshot.child("img3").getValue().toString()).into(dekorasi3);
                Picasso.with(PrettyDecoration.this).load(dataSnapshot.child("img4").getValue().toString()).into(dekorasi4);
                Picasso.with(PrettyDecoration.this).load(dataSnapshot.child("img5").getValue().toString()).into(dekorasi5);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}