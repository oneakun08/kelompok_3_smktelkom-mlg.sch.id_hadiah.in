package in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;

import in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin.lihat.PaketActivity;

public class SgiftActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    Button cari;
    RadioGroup radio;
    EditText umur, butget;
    RadioButton laki, pr;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private Toolbar toolbar;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sgift);

        butget = findViewById(R.id.butget);
        umur = findViewById(R.id.umur);
        laki = findViewById(R.id.laki);
        pr = findViewById(R.id.pr);
        radio = findViewById(R.id.radio);


        mDrawerLayout = findViewById(R.id.drawer);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.Open, R.string.Close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        cari = findViewById(R.id.cari);
        cari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
                String Ref = "";
                int id = radio.getCheckedRadioButtonId();
                int mUmur = Integer.parseInt(umur.getText().toString());
                int mbj = Integer.parseInt(butget.getText().toString());
                if (mUmur <= 7 && id == R.id.pr && mbj < 500000) {
                    Ref = "spesialW";
                } else if (mUmur > 7 && id == R.id.laki && mbj < 500000) {
                    Ref = "spesialL";
                } else if (mUmur >= 8 && id == R.id.pr && mbj < 500000) {
                    Ref = "";
                } else if (mUmur >= 50) {

                }

                Intent i = new Intent(SgiftActivity.this, TampilActivity.class);
                i.putExtra("Ref", Ref);
                startActivity(i);
                finish();
            }
        });


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setNavigationViewListener();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.dashboard: {
                Toast.makeText(getApplicationContext(), "Daftar Aplikasi Dipilih", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
                return true;
            }
            case R.id.about: {
                Toast.makeText(getApplicationContext(), "Daftar Aplikasi Dipilih", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), AboutActivity.class);
                startActivity(i);
                finish();
                return true;
            }
            case R.id.mvp: {
                Toast.makeText(getApplicationContext(), "Daftar Aplikasi Dipilih", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
                startActivity(i);
                finish();
                return true;
            }
            case R.id.help: {
                Toast.makeText(getApplicationContext(), "Daftar Aplikasi Dipilih", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), HelpActivity.class);
                startActivity(i);
                finish();
                return true;
            }
            case R.id.ms: {
                Toast.makeText(getApplicationContext(), "Daftar Aplikasi Dipilih", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), PaketActivity.class);
                startActivity(i);
                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void setNavigationViewListener() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //  View header = navigationView.inflateHeaderView(R.layout.header);
    }
}
