package in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.hadiah.kelompok_3.id.sch.smktelkom_mlg.hadiahin.model.tampilMode;

/**
 * Created by Skaha_AM on 12-May-18.
 */

public class tampilAdapter extends RecyclerView.Adapter<tampilAdapter.ViewHolder> {
    ArrayList<tampilMode> tampilModes = new ArrayList<>();
    Context context;

    public tampilAdapter(ArrayList<tampilMode> tampilModes, Context context) {
        this.tampilModes = tampilModes;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tampil, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.judul.setText(tampilModes.get(position).judul);
        holder.deskripsi.setText(tampilModes.get(position).desc);

        Picasso.with(context).load(tampilModes.get(position).img).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return tampilModes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView judul, deskripsi;
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            judul = itemView.findViewById(R.id.judul1);
            deskripsi = itemView.findViewById(R.id.desc1);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }
}
